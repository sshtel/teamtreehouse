
var http = require("http");

var username = "wonderjung";

function printMessage(username, badgeCount, points){
	var message = username + " has " + badgeCount + " total badge and " + points + "points in JS";
	console.log(message);
}

function printError(error){
	console.error(error.message);	
}


function getProfile(username){
	var request  = http.get("http://teamtreehouse.com/wonderjung.json", function(response){
		var body = "";

		response.on('data', function (chunk){
			body += chunk;
		});

		response.on('end', function(){
			console.log(body);
			if(response.statusCode === 200){
				try{
				var profile = JSON.parse(body);
				printMessage(username, profile.badges.length, profile.points.JavaScript);
				console.dir(profile);
				} catch(error) {
					printError(error);
				}
			}
			else {
				printError({message: "There was an error getting the profile for " + username + 
					http.STATUS_CODES[response.statusCode] + ")" });
			}
		});

		console.log(response.statusCode);
	});
}

request.on("error", function(error){
	console.error(error.message);
});


module.exports.get = getProfile;